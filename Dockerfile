FROM node:6

# Create app directory
WORKDIR /apitechu

#RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . /apitechu

EXPOSE 3003
CMD [ "node", "server.js" ]
