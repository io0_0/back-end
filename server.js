require('dotenv').config();
const express = require('express');
const app = express();
const BASE_URL = '/techu/v1/';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu6db/collections/'
const apikeyMLab = 'apiKey=' + process.env.API_KEY;
const port = process.env.PORT || 3000;
const body_parser = require('body-parser');
const request_json = require('request-json');
const cors = require('cors');  // Instalar dependencia 'cors' con npm
app.use(cors());
app.options('*', cors());
app.use(body_parser.json());
app.listen(port);



console.log("escuchando en el puerto" + port);
const usuarios = require('./user.json');
var client = request_json.createClient(URL_MLAB);
const fieldParamExclude = 'f={"_id":0}&';


app.get(BASE_URL + 'usuarios', function(req, resp) {

  var respuesta = client.get('users?' + fieldParamExclude + apikeyMLab,
    function(err, respuestaMLab, body) {
      console.log('Error: ' + err);
      console.log('Respuesta MLab: ' + respuestaMLab);
      console.log('Body: ' + JSON.stringify(body));
      var response = {};
      if (err) {
        response = {
          "msg": "Error al recuperar users de mLab."
        };
        resp.status(500);
      } else {
        if (body.length > 0) {
          response = body;
        } else {
          response = {
            "msg": "Usuario no encontrado."
          };
          resp.status(404);
        }
      }
      resp.send(response);
    });

});

app.get(BASE_URL + 'usuarios/:id', function(req, resp) {
  let pos = req.params.id;
  let queryFieḷd = 'q={"id":' + pos + '}&';
  var respuesta = client.get('users?' + queryFieḷd + fieldParamExclude + apikeyMLab,
    function(err, respuestaMLab, body) {
      console.log('Error: ' + err);
      console.log('Respuesta MLab: ' + respuestaMLab);
      console.log('Body: ' + JSON.stringify(body));

      var response = {};
      if (err) {
        response = {
          "msg": "Error al recuperar el usuario de mLab."
        }
        resp.status(500);
      } else {
        if (body.length > 0) {
          response = body;
        } else {
          response = {
            "msg": "Usuario no encontrado."
          };
          resp.status(404);
        }
      }
      resp.send(response);
    });

});

app.get(BASE_URL + 'usuarios/:id/accounts', function(req, resp) {
  let pos = req.params.id;
  let queryFieḷd = 'q={"id":' + pos + '}&';
  let respuesta = client.get('users?' + queryFieḷd + fieldParamExclude + apikeyMLab,
    function(err, respuestaMLab, body) {
      console.log('Error: ' + err);
      console.log('Respuesta MLab: ' + respuestaMLab);
      console.log('Body: ' + JSON.stringify(body));

      var response = {};
      if (err) {
        response = {
          "msg": "Error al recuperar el usuario de mLab."
        }
        resp.status(500);
      } else {
        if (body.length > 0) {
          response = body;
        } else {
          response = {
            "msg": "Usuario no encontrado."
          };
          resp.status(404);
        }
      }
      resp.send(response[0].accounts);
    });
});

app.post(BASE_URL + 'lel', function(req, resp) {
  console.log("aea estam os en post");
  client.get('users?' + fieldParamExclude + apikeyMLab,
    function(err, respuestaMLab, body) {
      console.log('Error: ' + err);
      console.log('Respuesta MLab: ' + respuestaMLab);
      console.log('Body: ' + JSON.stringify(body));
      let tam = 0;
      if (err) {
        response = {
          "msg": "Error al recuperar el usuario de mLab."
        };
        resp.status(500);
      } else {
        tam = body.length;
        let objeto = {
          "id_user": tam + 1,
          "first_name": req.body.first_name,
          "last_name": req.body.last_name,
          "email": req.body.email,
          "password": req.body.password
        };
        console.log(objeto);
        guardarMLab(objeto);
        resp.status(201).send(objeto);
      }
    });

});

app.put(BASE_URL + 'usuarios/:id', function(req, resp) {
  var id = req.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  client.get('users?' + apikeyMLab,
    function(error, respuestaMLab, body) {
      var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
      client.put(baseMLabUrl + 'users?q={"id": ' + id + '}&' + apikeyMLab, JSON.parse(cambio),
        function(error, respuestaMLab, body) {
          console.log("body:" + body);
          res.send(body);
        });
    });
  resp.status(201).send(objeto);

});

app.delete(BASE_URL + "usuarios/:id",
  function(req, res){
    var id = Number.parseInt(req.params.id);
    var query = 'q={"id":' + id + '}&';
    client.put("users?" + query + apikeyMLab, [{}],
        function(err, resMLab, body) {
          var response = !err ? body : {
            "msg" : "Error borrando usuario."
          }
        res.send(response);
      }
    );
  });


app.post(BASE_URL + "login",
  function(req, res) {
    console.log("POST /colapi/v3/login");
    let email = req.body.email;
    let pass = req.body.password;
    let queryString = 'q={"email":"' + email + '","phone":"' + pass + '"}&';
    let limFilter = 'l=1&';
    client.get('users?' + queryString + limFilter + apikeyMLab,
      function(error, respuestaMLab, body) {
        if (!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            client.put('users?q={"id": ' + body[0].id + '}&' + apikeyMLab, JSON.parse(login),
              //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                res.send({
                  'msg': 'Login correcto',
                  'user': body[0].email,
                  'userid': body[0].id,
                  'name': body[0].first_name
                });
                // If bodyPut.n == 1, put de mLab correcto
              });
          } else {
            res.status(404).send({
              "msg": "Usuario no válido."
            });
          }
        } else {
          res.status(500).send({
            "msg": "Error en petición a mLab."
          });
        }
      });
  });



//Method POST logout
app.post(BASE_URL + "logout",
  function(req, res) {
    console.log("POST /colapi/v3/logout");
    var email = req.body.email;
    var queryString = 'q={"email":"' + email + '","logged":true}&';
    console.log(queryString);
    client.get('users?' + queryString + apikeyMLab,
      function(error, respuestaMLab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        console.log(respuesta);
        if (!error) {
          if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            client.put('users?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(logout),
              //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
              function(errPut, resPut, bodyPut) {
                res.send({
                  'msg': 'Logout correcto',
                  'user': respuesta.email
                });
                // If bodyPut.n == 1, put de mLab correcto
              });
          } else {
            res.status(404).send({
              "msg": "Logout failed!"
            });
          }
        } else {
          res.status(500).send({
            "msg": "Error en petición a mLab.",
            "error":error
          });
        }
      });
  })




//utility functions
function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if (err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    });

}
// Petición GET con Query String (req.query)


function guardarMLab(obj) {

  client.post("users?" + apikeyMLab, obj,
    function(error, respuestaMLab, body) {});

};
